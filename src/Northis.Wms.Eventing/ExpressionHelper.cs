﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Northis.Common.Extensions;
using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing
{
	/// <summary>
	/// Представляет вспомогательный класс для получения имени и значения из выражения.
	/// </summary>
	public class ExpressionHelper
	{
		#region Public
		/// <summary>
		/// Возвращает отображаемое имя и значение выражения <paramref name="expression" />.
		/// </summary>
		/// <param name="expression">Выражение.</param>
		/// <returns>Пара: отображаемое имя и значение.</returns>
		/// <exception cref="System.ArgumentException">Expression body не содержит MemberExpression</exception>
		public static Tuple<string, string> GetDisplayValue(Expression<Func<object>> expression)
		{
			string key;
			var value = expression.Compile()();
			var @enum = value as Enum; // TODO придумать что-то симпатичнее
			if (@enum != null)
			{
				value = @enum.GetDescription();
			}

			MemberExpression memberExpression = null;
			memberExpression = expression.Body as MemberExpression;
			if (memberExpression == null)
			{
				var unary = expression.Body as UnaryExpression;
				memberExpression = unary?.Operand as MemberExpression;
			}
			if (memberExpression == null)
				throw new ArgumentException(Resources.StrError_InvalidExpression);

			var attribute = Attribute.GetCustomAttribute(memberExpression.Member, typeof(DisplayAttribute)) as DisplayAttribute;
			if (attribute == null)
			{
				key = memberExpression.Member.Name;
			}
			else
			{
				key = attribute.GetName();
			}
			return new Tuple<string, string>(key, value?.ToString());
		}
		#endregion
	}
}
