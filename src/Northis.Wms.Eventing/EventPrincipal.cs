﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using Newtonsoft.Json;
using Northis.Wms.Common.Enums;
using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing
{
	/// <summary>
	/// Представляет объект, инициировавший событие.
	/// </summary>
	[Serializable]
	public class EventPrincipal
	{
		#region Properties
		/// <summary>
		/// Возвращает или устанавливает IP-адрес хоста.
		/// </summary>
		/// <value>
		/// IP-адрес.
		/// </value>
		[Display(Name = "IPAddress", ResourceType = typeof(Resources))]
		[JsonConverter(typeof(IPAddressConverter))]
		public IPAddress Address
		{
			get;
			set;
		}

		/// <summary>
		/// Возвращает или устанавливает тип аутентификации.
		/// </summary>
		/// <value>
		/// Тип аутентификации.
		/// </value>
		[Display(Name = "AuthenticationType", ResourceType = typeof(Resources))]
		public AuthenticationType AuthenticationType
		{
			get;
			set;
		}

		/// <summary>
		/// Возвращает или устанавливает полное имя.
		/// </summary>
		/// <value>
		/// Полное имя.
		/// </value>
		[Display(Name = "PrincipalFullName", ResourceType = typeof(Resources))]
		public string FullName
		{
			get;
			set;
		}

		/// <summary>
		/// Возвращает или устанавливает имя хоста.
		/// </summary>
		/// <value>
		/// Имя хоста.
		/// </value>
		[Display(Name = "HostName", ResourceType = typeof(Resources))]
		public string HostName
		{
			get;
			set;
		}

		/// <summary>
		/// Возвращает или устанавливает имя объекта.
		/// </summary>
		/// <value>
		/// Имя.
		/// </value>
		[Display(Name = "PrincipalName", ResourceType = typeof(Resources))]
		public string Name
		{
			get;
			set;
		}
		#endregion
	}
}
