﻿namespace Northis.Wms.Eventing.Bus
{
	/// <summary>
	/// Представляет шину сообщений от системы слежения.
	/// </summary>
	public interface IOperationEventBus : IEventPublisher<IOperationEvent>
	{
		#region Overridable
		/// <summary>
		/// Публикует в шину сообщение.
		/// </summary>
		/// <param name="event">публикуемое сообщение.</param>
		void Publish(IOperationEvent @event);
		#endregion
	}
}
