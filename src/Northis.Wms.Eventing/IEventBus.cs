﻿using System.Threading;
using System.Threading.Tasks;
using Northis.CQRS.Eventing;

namespace Northis.Wms.Eventing
{
	/// <summary>
	/// Представляет механизм публикации событий для обработки.
	/// </summary>
	public interface IEventBus
	{
		/// <summary>
		/// Публикует событие для обработки.
		/// </summary>
		/// <param name="event">Сообщение для публикации.</param>
		void Publish(IEvent @event);

		/// <summary>
		/// Публикует событие для обработки асинхронно.
		/// </summary>
		/// <param name="event">Событие для публикации.</param>
		/// <param name="token">Маркер отмены.</param>
		/// <returns>Асинхронная операция.</returns>
		Task PublishAsync(IEvent @event, CancellationToken token = default(CancellationToken));
	}
}