﻿using System.Collections.Generic;
using Northis.CQRS.Eventing;

namespace Northis.Wms.Eventing
{
	/// <summary>
	/// Представляет интерфейс издателя событий.
	/// </summary>
	public interface IEventPublisher<out TEvent> where TEvent : class, IEvent
	{
		#region Properties
		/// <summary>
		/// Возвращает публикуемые события.
		/// </summary>
		/// <value>Перечисление событий.</value>
		IReadOnlyCollection<TEvent> Events
		{
			get;
		}
		#endregion

		#region Overridable
		/// <summary>
		/// Очищает список публикуемых событий <see cref="Events" />
		/// </summary>
		void Clear();
		#endregion
	}
}
