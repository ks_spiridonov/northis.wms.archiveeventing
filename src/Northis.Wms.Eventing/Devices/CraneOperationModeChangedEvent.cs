﻿using System;
using System.ComponentModel;
using System.Diagnostics.Contracts;

using Northis.Wms.Common.Enums;
using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing.Devices
{
	/// <summary>
	/// Представляет событие, возникающее при смене режима работы крана.
	/// </summary>
	[Serializable]
	[DisplayName("Смена режима определения операций крана")]
	public class CraneOperationModeChangedEvent : DeviceEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="DeviceEvent" />.
		/// </summary>
		/// <param name="deviceData">Данные по устройству.</param>
		/// <param name="isManualMode">Ручной режим работы крана.</param>
		public CraneOperationModeChangedEvent(WarehouseObjectFlatDto deviceData, bool isManualMode)
			: base(deviceData)
		{
			Contract.Requires<ArgumentException>(deviceData.Type == WarehouseObjectType.Crane);
			IsManualMode = isManualMode;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает значение, работает ли кран в ручном режиме.
		/// </summary>
		/// <value>
		/// <c>true</c> если кран работает в ручном режиме; иначе, <c>false</c>.
		/// </value>
		public bool IsManualMode
		{
			get;
		}

		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				return string.Format(Resources.StrCraneManualModeChanged, DeviceData, GetModeDescription());
			}
		}
		#endregion

		#region Private
		private string GetModeDescription()
		{
			if (IsManualMode)
			{
				return Resources.CraneManualMode;
			}

			return Resources.CraneAutomaticMode;
		}
		#endregion
	}
}
