﻿using System;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using Northis.Common.Extensions;
using Northis.Wms.Common.Enums;
using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing.Devices
{
	/// <summary>
	/// Представляет событие, возникающее при смене режима работы крана.
	/// </summary>
	[Serializable]
	[DisplayName("Смена режима работы крана")]
	public class CraneModeChangedEvent : DeviceEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="DeviceEvent" />.
		/// </summary>
		/// <param name="deviceData">Данные по устройству.</param>
		/// <param name="mode">Режим работы крана.</param>
		public CraneModeChangedEvent(WarehouseObjectFlatDto deviceData, CraneMode mode)
			: base(deviceData)
		{
			Contract.Requires<ArgumentException>(deviceData.Type == WarehouseObjectType.Crane);
			Mode = mode;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает режим работы крана.
		/// </summary>
		public CraneMode Mode
		{
			get;
		}
		#endregion

		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				return string.Format(Resources.StrCraneModeChanged, DeviceData, Mode.GetDescription());
			}
		}
	}
}
