﻿using System;
using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing.Devices
{
	/// <summary>
	/// Представляет базовый тип события, связанного с устройством.
	/// </summary>
	[Serializable]
	public abstract class DeviceEvent : OperationEvent, IDeviceOperationEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="DeviceEvent" />.
		/// </summary>
		/// <param name="deviceData">Данные по устройству.</param>
		protected DeviceEvent(WarehouseObjectFlatDto deviceData)
		{
			DeviceData = deviceData;
		}
		#endregion

		#region IDeviceOperationEvent members
		/// <summary>
		/// Возвращает данные об устройстве в рамках операции.
		/// </summary>
		/// <value>Данные о кране.</value>
		public WarehouseObjectFlatDto DeviceData
		{
			get;
		}
		#endregion

		#region IOperationEvent members
		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				return string.Format(Resources.StrDeviceEventSummary, DeviceData, OperationDisplayName);
			}
		}
		#endregion
	}
}
