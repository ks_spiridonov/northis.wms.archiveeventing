﻿using System;
using System.ComponentModel;

namespace Northis.Wms.Eventing.Devices
{
	/// <summary>
	/// Представляет событие, возникающее, когда телега покидает склад.
	/// </summary>
	/// <seealso cref="DeviceEvent" />
	[Serializable]
	[DisplayName("Выход телеги со склада")]
	public class TransferCartWarehouseLeftEvent : DeviceEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="DeviceEvent" />.
		/// </summary>
		/// <param name="deviceData">Данные по устройству.</param>
		public TransferCartWarehouseLeftEvent(WarehouseObjectFlatDto deviceData)
			: base(deviceData)
		{
		}
		#endregion
	}
}
