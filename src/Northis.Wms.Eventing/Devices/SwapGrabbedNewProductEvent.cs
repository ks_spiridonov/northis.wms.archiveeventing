﻿using System;
using System.ComponentModel;

using Northis.Wms.Eventing.ProductOperationEvents;
using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing.Devices
{
	/// <summary>
	/// Представляет событие, возникающее, когда машинист заменил ЕП, захваченную краном, на неизвестную системе.
	/// </summary>
	/// <seealso cref="DeviceEvent" />
	[Serializable]
	[DisplayName("Замена ЕП в кране")]
	public class SwapGrabbedNewProductEvent : DeviceEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="SwapGrabbedProductEvent" />.
		/// </summary>
		/// <param name="craneData">Данные крана в рамках операции.</param>
		public SwapGrabbedNewProductEvent(WarehouseObjectFlatDto craneData)
			: base(craneData)
		{
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает данные об устройстве в рамках операции.
		/// </summary>
		/// <value>Данные о кране.</value>
		public WarehouseObjectFlatDto DeviceData
		{
			get;
		}

		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				return string.Format(Resources.StrSwapGrabbedProduct, DeviceData, Resources.StrNewProduct);
			}
		}
		#endregion
	}
}
