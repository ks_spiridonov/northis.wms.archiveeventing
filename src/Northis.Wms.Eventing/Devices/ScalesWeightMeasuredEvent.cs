﻿using System;
using System.ComponentModel;
using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing.Devices
{
	/// <summary>
	/// Представляет событие, возникающее при взвешивании.
	/// </summary>
	[Serializable]
	[DisplayName("Взвешивание")]
	public class ScalesWeightMeasuredEvent : DeviceEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="ScalesWeightMeasuredEvent" />.
		/// </summary>
		/// <param name="deviceData">Данные по устройству.</param>
		/// <param name="measuredWeight">Установленный вес (кг).</param>
		public ScalesWeightMeasuredEvent(WarehouseObjectFlatDto deviceData, float measuredWeight) : base(deviceData)
		{
			MeasuredWeight = measuredWeight;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает значение установленного веса на весах.
		/// </summary>
		/// <value>
		/// Значение веса на весах, кг.
		/// </value>
		public float MeasuredWeight
		{
			get;
		}
		#endregion

		#region IOperationEvent members
		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				return string.Format(Resources.StrWeighEventDescription, DeviceData, MeasuredWeight);
			}
		}
		#endregion
	}
}
