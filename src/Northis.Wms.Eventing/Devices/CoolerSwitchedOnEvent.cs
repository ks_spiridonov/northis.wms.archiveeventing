﻿using System;
using System.ComponentModel;

namespace Northis.Wms.Eventing.Devices
{
	/// <summary>
	/// Представляет событие, возникающее, когда УУОР включается.
	/// </summary>
	/// <seealso cref="DeviceEvent" />
	[Serializable]
	[DisplayName("УУОР включен")]
	public class CoolerSwitchedOnEvent : DeviceEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="DeviceEvent" />.
		/// </summary>
		/// <param name="deviceData">Данные по устройству.</param>
		public CoolerSwitchedOnEvent(WarehouseObjectFlatDto deviceData)
			: base(deviceData)
		{
		}
		#endregion
	}
}
