﻿using Northis.CQRS.Eventing;

namespace Northis.Wms.Eventing
{
	/// <summary>
	/// Интерфейс обработчика событий.
	/// </summary>
	public interface IEventHandler
	{
		
	}

	/// <summary>
	/// Представляет механизм обработки событий типа <typeparamref name="T"/>.
	/// </summary>
	/// <typeparam name="T">Тип события.</typeparam>
	/// <seealso cref="IEventHandler" />
	public interface IEventHandler<in T> : IEventHandler where T : IEvent
	{
		/// <summary>
		/// Обрабатывает указанное событие.
		/// </summary>
		/// <param name="event">Событие.</param>
		void Handle(T @event);
	}
}