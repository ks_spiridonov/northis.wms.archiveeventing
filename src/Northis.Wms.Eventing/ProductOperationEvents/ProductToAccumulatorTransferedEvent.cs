﻿using System;
using System.ComponentModel;

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	[Serializable]
	[DisplayName("Переход в накопитель")]
	public class ProductToAccumulatorTransferedEvent : ProductOperationEvent, IDeviceOperationEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="ProductOperationEvent" />.
		/// </summary>
		/// <param name="productData">Данные единицы продукции в рамках операции.</param>
		/// <param name="productOwnerData">Данные объекта, которому принадлежит продукция в рамках операции.</param>
		/// <param name="accumulatorData">Данные накопителя.</param>
		public ProductToAccumulatorTransferedEvent(WarehouseObjectFlatDto productData,
			WarehouseObjectFlatDto productOwnerData,
			WarehouseObjectFlatDto accumulatorData)
			: base(productData, productOwnerData)
		{
			DeviceData = accumulatorData;
		}
		#endregion

		#region IDeviceOperationEvent members
		/// <summary>
		/// Возвращает данные об устройстве в рамках операции.
		/// </summary>
		/// <value>Данные об устройстве.</value>
		public WarehouseObjectFlatDto DeviceData
		{
			get;
		}
		#endregion
	}
}
