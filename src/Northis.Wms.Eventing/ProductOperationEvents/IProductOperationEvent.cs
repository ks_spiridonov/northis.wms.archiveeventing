﻿namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет событие на операцию с продукцией системы слежения.
	/// </summary>
	/// <seealso cref="IOperationEvent" />
	public interface IProductOperationEvent : IOperationEvent
	{
		/// <summary>
		/// Возвращает данные единицы продукции в рамках операции.
		/// </summary>
		/// <value>Данные единицы продукции.</value>
		WarehouseObjectFlatDto ProductData { get; }

		/// <summary>
		/// Возвращает данные объекта, которому принадлежит продукция в рамках операции.
		/// </summary>
		/// <value>Данные объекта, которому принадлежит продукция.</value>
		/// <remarks>Представляет конечный объект в структуре склада, в котором находится продукция в рамках операции (пролет, зона, устройство, место постановки и т.п.). Исключение: едница хранения.</remarks>
		WarehouseObjectFlatDto ProductOwnerData { get; }
	}
}