﻿using System;
using System.ComponentModel;

using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет событие, возникающее, когда машинист заменил ЕП, захваченную краном.
	/// </summary>
	/// <seealso cref="ProductOperationEvent" />
	[Serializable]
	[DisplayName("Замена ЕП в кране")]
	public class SwapGrabbedProductEvent : ProductOperationEvent, IDeviceOperationEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="SwapGrabbedProductEvent" />.
		/// </summary>
		/// <param name="productData">Данные единицы продукции в рамках операции.</param>
		/// <param name="productOwnerData">Данные объекта, которому принадлежит продукция в рамках операции.</param>
		/// <param name="craneData">Данные крана в рамках операции.</param>
		public SwapGrabbedProductEvent(WarehouseObjectFlatDto productData, WarehouseObjectFlatDto productOwnerData, WarehouseObjectFlatDto craneData)
			: base(productData, productOwnerData)
		{
			DeviceData = craneData;
		}
		#endregion

		#region IDeviceOperationEvent members
		/// <summary>
		/// Возвращает данные об устройстве в рамках операции.
		/// </summary>
		/// <value>Данные о кране.</value>
		public WarehouseObjectFlatDto DeviceData
		{
			get;
		}
		#endregion

		#region IOperationEvent members
		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				return string.Format(Resources.StrSwapGrabbedProduct, DeviceData, ProductData, ProductOwnerData);
			}
		}
		#endregion
	}
}
