﻿using System;
using System.ComponentModel;
using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет событие, возникающее, когда продукция захвачена краном.
	/// </summary>
	/// <seealso cref="ProductOperationEvent" />
	[Serializable]
	[DisplayName("Поднятие краном")]
	public class ProductGrabbedEvent : ProductOperationEvent, IDeviceOperationEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="ProductGrabbedEvent" />.
		/// </summary>
		/// <param name="productData">Данные единицы продукции в рамках операции.</param>
		/// <param name="productOwnerData">Данные объекта, которому принадлежит продукция в рамках операции.</param>
		/// <param name="craneData">Данные крана в рамках операции.</param>
		/// <param name="isManual">Если установлено <c>true</c>, операция выполнена в ручном режиме.</param>
		public ProductGrabbedEvent(WarehouseObjectFlatDto productData, WarehouseObjectFlatDto productOwnerData, WarehouseObjectFlatDto craneData, bool isManual = false)
			: base(productData, productOwnerData)
		{
			DeviceData = craneData;
			IsManual = isManual;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает данные об устройстве в рамках операции.
		/// </summary>
		/// <value>Данные о кране.</value>
		public WarehouseObjectFlatDto DeviceData
		{
			get;
			private set;
		}

		/// <summary>
		/// Возвращает значение, указывающее, что операция выполнена в ручном режиме.
		/// </summary>
		/// <value><c>true</c> если операция выполнена в ручном режиме; иначе, <c>false</c>.</value>
		public bool IsManual
		{
			get;
			private set;
		}
		#endregion

		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				return string.Format(Resources.StrProductGrabbedEventDescription, ProductData, DeviceData, ProductOwnerData, IsManual ? Resources.StrManualMode : Resources.StrAutomaticMode);
			}
		}
	}
}
