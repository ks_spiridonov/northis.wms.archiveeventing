﻿using System;
using System.ComponentModel;

using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет событие, возникающее, когда приходят данные из MES на продукцию.
	/// </summary>
	/// <seealso cref="ProductOperationEvent" />
	[Serializable]
	[DisplayName("Идентификация данными из MES")]
	public class ProductIdentifiedByMesEvent : ProductOperationEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр <see cref="ProductIdentifiedByMesEvent" />.
		/// </summary>
		/// <param name="productData">Данные единицы продукции в рамках операции.</param>
		/// <param name="productOwnerData">Данные объекта, которому принадлежит продукция в рамках операции.</param>
		public ProductIdentifiedByMesEvent(WarehouseObjectFlatDto productData, WarehouseObjectFlatDto productOwnerData)
			: base(productData, productOwnerData)
		{
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				return string.Format(Resources.StrIdentifiedMes, ProductData);
			}
		}
		#endregion
	}
}
