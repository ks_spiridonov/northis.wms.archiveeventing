﻿using System;
using System.ComponentModel;
using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет событие, возникающее при подтверждении веса.
	/// </summary>
	/// <seealso cref="Northis.Wms.Eventing.ProductOperationEvents.ProductOperationEvent" />
	[Serializable]
	[DisplayName("Подтверждение веса")]
	public class ProductWeightConfirmedEvent : ProductOperationEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="ProductWeightConfirmedEvent" />.
		/// </summary>
		/// <param name="weight">Подтвержденный вес (кг).</param>
		/// <param name="productData">Данные единицы продукции в рамках операции.</param>
		/// <param name="productOwnerData">Данные объекта, которому принадлежит продукция в рамках операции.</param>
		public ProductWeightConfirmedEvent(float weight,
			WarehouseObjectFlatDto productData,
			WarehouseObjectFlatDto productOwnerData)
			: base(productData, productOwnerData)
		{
			Weight = weight;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает подтвержденный вес продукции.
		/// </summary>
		/// <value>
		/// Вес продукции, кг.
		/// </value>
		public float Weight
		{
			get;
		}

		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				return string.Format(Resources.StrProductWeightConfirmedDescription, ProductData, Weight);
			}
		}
		#endregion
	}
}
