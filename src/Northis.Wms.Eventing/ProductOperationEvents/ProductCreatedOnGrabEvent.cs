﻿using System;
using System.ComponentModel;

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет событие создания продукта краном в контексте операции захвата краном.
	/// <seealso cref="ProductCreatedEvent" />
	/// <seealso cref="IDeviceOperationEvent" />"/>
	/// </summary>
	[Serializable]
	[DisplayName("Прибытие на склад")]
	public class ProductCreatedOnGrabEvent : ProductCreatedEvent, IDeviceOperationEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует экземпляр типа <see cref="ProductCreatedOnGrabEvent" />
		/// </summary>
		/// <param name="productData">Данные о продукте.</param>
		/// <param name="productOwnerData">Данные о месте откуда был выполнен захват продукции.</param>
		/// <param name="deviceData">Данные о устройстве, которое создало продукт.</param>
		public ProductCreatedOnGrabEvent(WarehouseObjectFlatDto productData, WarehouseObjectFlatDto productOwnerData, WarehouseObjectFlatDto deviceData)
			: base(productData, productOwnerData)
		{
			DeviceData = deviceData;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает устройство которое создало продукт.
		/// </summary>
		public WarehouseObjectFlatDto DeviceData
		{
			get;
		}
		#endregion
	}
}
