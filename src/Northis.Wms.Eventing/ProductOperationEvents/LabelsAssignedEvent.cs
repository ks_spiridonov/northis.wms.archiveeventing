﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет событие, возникающее в момент назначения меток на продукцию.
	/// </summary>
	/// <seealso cref="ProductOperationEvent" />
	[Serializable]
	[DisplayName("Добавление меток")]
	public class LabelsAssignedEvent : ProductOperationEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="LabelsAssignedEvent" />.
		/// </summary>
		/// <param name="productData">Данные единицы продукции в рамках операции.</param>
		/// <param name="productOwnerData">Данные объекта, которому принадлежит продукция в рамках операции.</param>
		public LabelsAssignedEvent(WarehouseObjectFlatDto productData, WarehouseObjectFlatDto productOwnerData) : base(productData, productOwnerData)
		{
			Labels = new List<string>();
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает коллекцию назначенных текстовых меток.
		/// </summary>
		/// <value>The labels.</value>
		public ICollection<string> Labels
		{
			get;
		}
		#endregion

		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				var stringBuilder = new StringBuilder();
				stringBuilder.AppendLine(string.Format(Resources.StrLabelsAssignedEventDescription, ProductData, string.Join("; ", Labels)));
				return stringBuilder.ToString();
			}
		}
	}
}
