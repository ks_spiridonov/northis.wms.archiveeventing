﻿using System;
using System.ComponentModel;

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет событие, возникающее, когда создается новая ЕП.
	/// </summary>
	/// <seealso cref="ProductOperationEvent" />
	[Serializable]
	[DisplayName("Прибытие на склад")]
	public class ProductCreatedEvent : ProductOperationEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="ProductOperationEvent" />.
		/// </summary>
		/// <param name="productData">Данные единицы продукции в рамках операции.</param>
		/// <param name="productOwnerData">Данные объекта, которому принадлежит продукция в рамках операции.</param>
		public ProductCreatedEvent(WarehouseObjectFlatDto productData, WarehouseObjectFlatDto productOwnerData)
			: base(productData, productOwnerData)
		{
			
		}
		#endregion
	}
}
