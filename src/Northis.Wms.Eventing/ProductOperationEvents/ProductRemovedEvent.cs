﻿using System;
using System.ComponentModel;
using Northis.Wms.Eventing.Properties;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет событие, возникающее, когда удаляется ЕП.
	/// </summary>
	/// <seealso cref="ProductOperationEvent" />
	[Serializable]
	[DisplayName("Убытие продукции со склада")]
	public class ProductRemovedEvent : ProductOperationEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="ProductRemovedEvent" />.
		/// </summary>
		/// <param name="productData">Данные единицы продукции в рамках операции.</param>
		/// <param name="productOwnerData">Данные объекта, которому принадлежит продукция в рамках операции.</param>
		/// ///
		/// <param name="reason">Причина удаления.</param>
		public ProductRemovedEvent(WarehouseObjectFlatDto productData, WarehouseObjectFlatDto productOwnerData, string reason)
			: base(productData, productOwnerData)
		{
			Reason = reason;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает причину удаления.
		/// </summary>
		/// <value>Причина удаления.</value>
		public string Reason
		{
			get;
			private set;
		}

		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				return string.Format(Resources.StrProductRemovedEventDescription, ProductData, Reason);
			}
		}
		#endregion
	}
}
