﻿namespace Northis.Wms.Eventing
{
	/// <summary>
	/// Определяет ключи для параметров объектов.
	/// </summary>
	public static class WarehouseObjectKeys
	{
		#region Data

		#region Consts
		/// <summary>
		/// Ключ на отображаемое имя.
		/// </summary>
		public const string DisplayName = "DisplayName";
		/// <summary>
		/// Ключ на уникальное имя.
		/// </summary>
		public const string Name = "Name";
		/// <summary>
		/// Ключ на ГЗМ (для крана).
		/// </summary>
		public const string WorkMechanism = "WorkMechanism";
		/// <summary>
		/// Ключ на модель ГЗМ (для крана).
		/// </summary>
		public const string WorkMechanismModel = "Model";
		/// <summary>
		/// Ключ на тип ГЗМ (для крана).
		/// </summary>
		public const string WorkMechanismType = "Type";
		#endregion

		#endregion

		#region Nested types

		#region Type: CoilKeys
		public static class CoilKeys
		{
			#region Data

			#region Consts
			/// <summary>
			/// Ключ на позицию X.
			/// </summary>
			public const string GlobalPositionX = "GlobalPositionX";
			/// <summary>
			/// Ключ на позицию Y.
			/// </summary>
			public const string GlobalPositionY = "GlobalPositionY";
			/// <summary>
			/// Ключ на идентификатор MES.
			/// </summary>
			public const string MesId = "MesId";
			/// <summary>
			/// Ключ на номер партии.
			/// </summary>
			public const string BatchNumber = "BatchNumber";
			/// <summary>
			/// Ключ на номер рулона.
			/// </summary>
			public const string CoilNumber = "CoilNumber";
			#endregion

			#endregion
		}
		#endregion

		#endregion
	}
}
